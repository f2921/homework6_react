export const cartData = {
  showAddToCartModal: false,
  cartProduct: [],
  showDeleteModal: false,
  checkOutData: [],
  switch: true,
};

export function CartReducer(state = {}, action) {
  if (action.type == "addToCartModal") {
    return {
      ...state,
      showAddToCartModal: action.payload.showAddToCartModal,
    };
  }
  if (action.type == "deleteFromCartModal") {
    return {
      ...state,
      showDeleteModal: action.payload.showDeleteModal,
    };
  }
  if (action.type == "add-Cart-data") {
    return {
      ...state,
      cartProduct: action.payload.cartProduct,
    };
  }

  if (action.type == "deleteFromCart") {
    return {
      ...state,
      cartProduct: action.payload.cartProduct,
    };
  }

  if (action.type == "addCheckoutData") {
    return {
      ...state,
      checkOutData: action.payload.checkOutData,
    };
  }

  if (action.type == "changeSwitchData") {
    return {
      ...state,
      switch: action.payload.switch,
    };
  }

  return state;
}

export function getCartModal(state) {
  return state.cartdata.showAddToCartModal;
}

export function getDeleteCartModal(state) {
  return state.cartdata.showDeleteModal;
}

export function returnData(state) {
  return state.cartdata.cartProduct;
}

export function returnCheckoutData(state) {
  return state.cartdata.checkOutData;
}

export function getSwitch(state) {
  return state.cartdata.switch;
}

export function openModal(newData) {
  return {
    type: "addToCartModal",
    payload: {
      showAddToCartModal: newData,
    },
  };
}

export function openDeleteModal(newData) {
  return {
    type: "deleteFromCartModal",
    payload: {
      showDeleteModal: newData,
    },
  };
}

export function editCart(data) {
  return {
    type: "add-Cart-data",
    payload: {
      cartProduct: data,
    },
  };
}

export function deleteCart(data) {
  return {
    type: "deleteFromCart",
    payload: {
      cartProduct: data,
    },
  };
}

export function editCheckout(data) {
  return {
    type: "addCheckoutData",
    payload: {
      checkOutData: data,
    },
  };
}

export function editSwitch(data) {
  return {
    type: "changeSwitchData",
    payload: {
      switch: data,
    },
  };
}

export function showCartModal() {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.showAddToCartModal;
    return dispatch(openModal(!newData));
  };
}

export function showDeleteModal() {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.showDeleteModal;
    return dispatch(openDeleteModal(!newData));
  };
}
export function addToCart(product) {
  return (dispatch, getstate) => {
    const cartData = getstate().cartdata.cartProduct;
    const newData = cartData ? [...cartData, product] : product;
    localStorage.cartData = JSON.stringify(newData);
    return dispatch(editCart(newData));
  };
}

export function deleteFromCart(product) {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.cartProduct.filter(
      (item) => item.id !== product.id
    );
    localStorage.cartData = JSON.stringify(newData);
    return dispatch(deleteCart(newData));
  };
}

export function addCheckoutProduct(product) {
  return (dispatch, getstate) => {
    const checkoutData = getstate().cartdata.checkOutData;
    const newData = checkoutData ? [...checkoutData, product] : product;
    localStorage.checkoutData = JSON.stringify(newData);
    localStorage.removeItem("cartData");

    return dispatch(editCheckout(newData));
  };
}

export function changeSwitch() {
  return (dispatch, getstate) => {
    const newData = getstate().cartdata.switch;
    return dispatch(editSwitch(!newData));
  };
}
