export const productData = [];

export function productReducer(state = {}, action) {
  if (action.type == "get-products") {
    return {
      products: action.payload.products,
    };
  }
  return state;
}

export function getData(state) {
  return state.products;
}

function editProduct(newData) {
  return {
    type: "get-products",
    payload: {
      products: newData,
    },
  };
}

export function loadData() {
  return (dispatch, getstate) => {
    return fetch("data.json")
      .then((data) => data.json())
      .then((data) => dispatch(editProduct(data)));
  };
}
