import { useEffect, useState, createContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import Router from "./components/Routes";
import { getData, loadData } from "./Features/Products/productslice";
import ModalData from "./components/ModalData/ModalData";
import {
  showCartModal,
  addToCart,
  editCart,
  getDeleteCartModal,
  showDeleteModal,
  deleteFromCart,
  editCheckout,
  getCartModal,
  returnData,
  addCheckoutProduct,
  returnCheckoutData,
  getSwitch,
  changeSwitch,
} from "./Features/Carts/cartslice";
import {
  addToFav,
  returnFav,
  editFav,
  deleteFromFav,
} from "./Features/Favorites/favorites";

export const AppContext = createContext(null);

function App() {
  const { products } = useSelector(getData);
  const cartModal = useSelector(getCartModal);
  const deleteCartModal = useSelector(getDeleteCartModal);
  const cartData = useSelector(returnData);
  const favoriteData = useSelector(returnFav);
  const checkoutData = useSelector(returnCheckoutData);
  const switchData = useSelector(getSwitch);
  const dptch = useDispatch();
  const [checkItem, setCheckitem] = useState([]);

  useEffect(function () {
    dptch(loadData());
    if (localStorage.getItem("cartData")) {
      dptch(editCart(JSON.parse(localStorage.cartData)));
    }

    if (localStorage.getItem("favData")) {
      dptch(editFav(JSON.parse(localStorage.favData)));
    }

    if (localStorage.getItem("checkoutData")) {
      dptch(editCheckout(JSON.parse(localStorage.checkoutData)));
    }
  }, []);

  const checkItemData = (product) => {
    setCheckitem(product);
  };

  const openAddToCartModal = () => {
    dptch(showCartModal());
  };

  const openDeleteCartModal = () => {
    dptch(showDeleteModal());
  };

  const addToCartData = (product) => {
    dptch(addToCart(product));
  };

  const addToFavorite = (product) => {
    dptch(addToFav(product));
  };

  const deleteFavorite = (product) => {
    dptch(deleteFromFav(product));
  };

  const deleteFromCartData = (product) => {
    dptch(deleteFromCart(product));
  };

  const addcheckoutData = (product) => {
    const newData = cartData ? [...cartData, product] : product;
    dptch(addCheckoutProduct(newData));
  };

  const changeSwitchData = () => {
    dptch(changeSwitch());
  };

  const values = {
    products: products,
    showAddToCartModal: openAddToCartModal,
    checkItemData: checkItemData,
    cartData: cartData,
    favData: favoriteData,
    addToFav: addToFavorite,
    deleteFavorite: deleteFavorite,
    showDeleteModal: openDeleteCartModal,
    addcheckoutData: addcheckoutData,
    checkItem: checkItem,
    checkoutData: checkoutData,
    switchData: switchData,
    changeSwitchData: changeSwitchData,
  };

  console.log(11111, checkoutData);
  return (
    <AppContext.Provider value={values}>
      <div className="App">
        <Router />
        {cartModal && (
          <ModalData
            modalHeader="Do you want to add?"
            modalTitle="Cart Modal"
            modalText="Happy schopping!!!"
            background="blue"
            colorButtonOk="orange"
            colorButtonCancel="red"
            buttonName="Add"
            showAddToCartModal={openAddToCartModal}
            action={addToCartData}
            checkItem={checkItem}
          />
        )}
        {deleteCartModal && (
          <ModalData
            modalHeader="Do you want to delete?"
            modalTitle="Cart Delete Modal"
            modalText="Happy schopping!!!"
            background="blue"
            colorButtonOk="orange"
            colorButtonCancel="red"
            buttonName="Delete"
            showAddToCartModal={openDeleteCartModal}
            action={deleteFromCartData}
            checkItem={checkItem}
          />
        )}
      </div>
    </AppContext.Provider>
  );
}

export default App;
