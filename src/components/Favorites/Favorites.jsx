import { useContext } from "react";
import { BsStarFill } from "react-icons/bs";
import { AppContext } from "../../App";
export default function Favotites({}) {
  const { showAddToCartModal } = useContext(AppContext);
  const { checkItemData } = useContext(AppContext);
  const { favData } = useContext(AppContext);
  const { deleteFavorite } = useContext(AppContext);

  return (
    <div className="item-list">
      {favData.map((product) => {
        return (
          <div className="item" key={product.id}>
            <button
              className="star"
              onClick={() => {
                deleteFavorite(product);
              }}
            >
              <BsStarFill />
            </button>
            <div className="item__content">
              <div className="item__content--img-block">
                <img src={product.image} />
              </div>
              <div className="item__content--info-block">
                <h3 className="item-title">{product.title}</h3>
                <p className="item-price">{product.price} грн</p>
                <p>Артикул: {product.id}</p>
              </div>
            </div>
            <button
              className="cartBtn"
              onClick={() => {
                showAddToCartModal();
                checkItemData(product);
              }}
            >
              Add to cart
            </button>
          </div>
        );
      })}
    </div>
  );
}
