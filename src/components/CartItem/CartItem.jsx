import "./CartItem.scss";
import { useContext } from "react";
import { AppContext } from "../../App";

export default function CartItem() {
  const { checkItemData } = useContext(AppContext);
  const { cartData } = useContext(AppContext);
  const { showDeleteModal } = useContext(AppContext);
  return (
    <>
      {cartData.length > 0 &&
        cartData.map((product) => {
          return (
            <div className="item-cart" key={product.id}>
              <span
                className="close"
                onClick={() => {
                  showDeleteModal();
                  checkItemData(product);
                }}
              >
                &times;
              </span>
              <div className="item__content">
                <div className="item__content--img-block">
                  <img src={product.image} />
                </div>
                <div className="item__content--info-block">
                  <h3 className="item-title">{product.title}</h3>
                  <p className="item-price">{product.price} грн</p>
                  <p>Артикул: {product.id}</p>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
}
