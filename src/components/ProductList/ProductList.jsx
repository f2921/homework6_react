import "./ProductList.scss";
import Product from "../Product/Product";
import { useContext } from "react";
import { AppContext } from "../../App";

function ProductList({}) {
  const { products } = useContext(AppContext);
  const { favData } = useContext(AppContext);

  const checkFav = (data) => {
    return favData.some((fav) => {
      return fav.id === data.id;
    });
  };
  return (
    <div className="item-list">
      {products ? (
        products.map((product) => {
          return (
            <Product
              key={product.id}
              product={product}
              checkFav={checkFav(product)}
            />
          );
        })
      ) : (
        <h1>Product Count 0</h1>
      )}
    </div>
  );
}
export default ProductList;
