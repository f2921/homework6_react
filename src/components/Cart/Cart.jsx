import "./Cart.scss";
import Formochka from "../Formochka/formochka";
import { useContext } from "react";
import { AppContext } from "../../App";
import Table from "../Table/Table";
import CartItem from "../CartItem/CartItem";

export default function Cart() {
  const { cartData } = useContext(AppContext);
  const { changeSwitchData } = useContext(AppContext);
  const { switchData } = useContext(AppContext);
  return (
    <div className="cart-container">
      <h1>Product Count {cartData.length}</h1>
      <button
        onClick={() => {
          changeSwitchData();
        }}
      >
        Change
      </button>
      <div className="cart-section">
        <div className="cart-product-section">
          {switchData ? <Table /> : <CartItem />}
        </div>
        {cartData.length > 0 && <Formochka />}
      </div>
    </div>
  );
}
