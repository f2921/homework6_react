import "./Product.scss";
import { BsStarFill } from "react-icons/bs";
import { useContext } from "react";
import { AppContext } from "../../App";

function Product({ product, checkFav }) {
  const { image, title, price, id } = product;
  const { showAddToCartModal } = useContext(AppContext);
  const { checkItemData } = useContext(AppContext);
  const { addToFav } = useContext(AppContext);
  const { deleteFavorite } = useContext(AppContext);

  return (
    <div className="item">
      <button
        className={`star ${checkFav ? "active" : ""}`}
        onClick={() => {
          checkFav ? deleteFavorite(product) : addToFav(product);
        }}
      >
        <BsStarFill />
      </button>
      <div className="item__content">
        <div className="item__content--img-block">
          <img src={image} />
        </div>
        <div className="item__content--info-block">
          <h3 className="item-title">{title}</h3>
          <p className="item-price">{price} грн</p>
          <p>Артикул: {id}</p>
        </div>
      </div>
      <button
        className="cartBtn"
        onClick={() => {
          showAddToCartModal();
          checkItemData(product);
        }}
      >
        Add to cart
      </button>
    </div>
  );
}
export default Product;
