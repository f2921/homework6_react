import { Route, Routes } from "react-router-dom";
import Header from "../Header/Header";
import ProductList from "../ProductList/ProductList";
import Cart from "../Cart/Cart";
import Favotites from "../Favorites/Favorites";

export default function Router() {
  return (
    <Routes>
      <Route path="/" element={<Header />}>
        <Route index path="/" element={<ProductList />} />
        <Route path="/favorites" element={<Favotites />} />
        <Route path="/cart" element={<Cart />} />
      </Route>
    </Routes>
  );
}
