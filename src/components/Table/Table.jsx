import "./Table.scss";
import { useContext } from "react";
import { AppContext } from "../../App";

export default function Table() {
  const { checkItemData } = useContext(AppContext);
  const { cartData } = useContext(AppContext);
  const { showDeleteModal } = useContext(AppContext);
  return (
    <table className="customers">
      <tbody>
        <tr>
          <th>Number</th>
          <th>Name</th>
          <th>Price</th>
          <th>delete</th>
        </tr>
        {cartData.length > 0 &&
          cartData.map((product, index) => {
            return (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{product.title}</td>
                <td>{product.price} $</td>
                <td>
                  <span
                    className="closeTd"
                    onClick={() => {
                      showDeleteModal(product);
                      checkItemData(product);
                    }}
                  >
                    &times;
                  </span>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
}
