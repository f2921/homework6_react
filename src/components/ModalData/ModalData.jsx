import "./ModalData.scss";

export default function ModalData(props) {
  const background = props.background;
  const action = props.action;
  const { showAddToCartModal } = props;
  const { checkItem } = props;
  return (
    <div
      className="modal-background"
      onClick={() => {
        showAddToCartModal();
      }}
    >
      <div
        className="modalData"
        style={{ backgroundColor: background }}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div className="modal-header">
          <span
            className="close"
            onClick={() => {
              showAddToCartModal();
            }}
          >
            &times;
          </span>
          <h2 className="modalHeader">{props.modalHeader}</h2>
        </div>
        <div className="modal__content-container">
          <h3>{props.modalTitle}</h3>
          <p id="my-test-id">{props.modalText}</p>
          <div className="button-block">
            <button
              className="modal__btn"
              onClick={() => {
                action(checkItem);
                showAddToCartModal();
              }}
            >
              {props.buttonName}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
