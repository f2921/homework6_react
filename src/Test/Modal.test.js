import React from "react";
import ModalData from "../components/ModalData/ModalData";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

describe("modal it", () => {
  it("modal text", () => {
    render(<ModalData modalText="Happy schopping" />);
    expect(screen.getByText(/Happy schopping/)).toBeInTheDocument();
  });

  it("button", () => {
    const { getByRole } = render(<ModalData />);
    const buttonEl = getByRole("button");
    expect(buttonEl).toBeInTheDocument();
  });

  it("test modal func", () => {
    const AddToCartModal = jest.fn();
    const actionFunc = jest.fn();
    const { getByRole } = render(
      <ModalData action={actionFunc} showAddToCartModal={AddToCartModal} />
    );
    fireEvent.click(getByRole("button"));
    expect(AddToCartModal).toHaveBeenCalledTimes(1);
  });
});
